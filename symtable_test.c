#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<assert.h>
#include<vld.h>
#include "symtable.h"
static void SymTable_print(const char*pcKey, const void *pvValue,void *pvExtra)// function to print key and value
{
	char *k1,*v1;
	k1=(char*)pcKey;
	v1=(char*)pvValue;
	assert(pcKey);
	assert(pvValue);
	if(pvExtra==NULL)
	{
		printf("\n %s",k1);
		printf("\t %s",v1);
	}
}
int main()
{
	
	SymTable_t h=SymTable_new();	//creates a new symboltable h
	SymTable_t h1=SymTable_new();	//creates a new symboltable h1
	int count,contains,inc;
	char c[10000][100];
	//char **c;
	char s[10];
	void* check;
	int n=9899;
	clock_t start,end;
	start=clock();
	for(inc=1;inc<10000;inc++)	// for inserting 100 i/ps
	{
		sprintf(c[inc],"%d",inc);
		if(SymTable_put(h,c[inc],(void *)c[inc]))
			printf("\n Insertion succesful ");
		else
			printf("\n Key already exist or not enough memory :");
	}
	count=SymTable_getLength(h);	// call to SymTable_getLength to get the length of symboltable h
	printf("\n count: %d\n",count);
	sprintf(s,"%d",n);
	
	contains=SymTable_contains(h,(char *)s);	// call to SymTable_contains to check whether the key is present or not
	if(contains)
		printf("\n the key s present");
	else
		printf("\n the key s not present");
	
	printf(" \n the value of key is %s",(char *)SymTable_get(h,s));//call to SymTable_get to print the value corresponding to key  

	check=SymTable_replace(h,s,(void *)7);	// call to SymTable_replace function to replace a keys value with another one
	if(check)
		printf("\n replaced");
	else
		printf("\n its not present");

	check=SymTable_remove(h,s);	// call to SymTable_remove function to remove a binding from the symbol table
	if(check)
		printf("\n removed");
	else
		printf("\n not present");

	SymTable_map(h,SymTable_print,NULL);	// calls the symtable_map function to iterate through each bindings

	SymTable_free(h);	// call to SymTable_free function to free all memory used by symbol table h

	for(inc=1;inc<100;inc++)	// for inserting 100 i/ps
	{
		sprintf(c[inc],"%d",inc);
		if(SymTable_put(h1,c[inc],(void *)c[inc]))
			printf("\n Insertion succesful ");
		else
			printf("\n Key already exist or not enough memory :");
	}
	for(inc=1;inc<100;inc++)	// for checking SymTable_put function
	{
		sprintf(c[inc],"%d",inc);
		if(SymTable_put(h1,c[inc],(void *)c[inc]))
			printf("\n Insertion succesful ");
		else
			printf("\n Key already exist or not enough memory :");
	}

	SymTable_map(h1,SymTable_print,NULL);// to display all bindings

	contains=SymTable_contains(h1,(char *)s);	// call to SymTable_contains to check whether the key is present or not
	if(contains)
		printf("\n the key s present");
	else
		printf("\n the key s not present");

	count=SymTable_getLength(h1);

	printf("\n count: %d",count);

	check=SymTable_replace(h1,s,(void *)7);	// call to SymTable_replace function to replace a keys value with another one
	if(check)
		printf("\n replaced");
	else
		printf("\n its not present");

	check=SymTable_remove(h1,s);	// call to SymTable_remove function to remove a binding from the symbol table
	if(check)
		printf("\n removed");
	else
		printf("\n not present");

	SymTable_free(h1);
	end=clock();
	printf("\n total time taken : %lf",(double)((end-start)/CLOCKS_PER_SEC)); // prints the time taken for execution

	return 0;

}

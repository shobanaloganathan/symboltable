// implementation of hash function
#include <stdio.h>
#include<assert.h>
#include "hash.h"
int symtable_hash(const char *pckey,int ibucketcount) // calculates a hash value depending on the bucket size and returns the hash value
{
	enum 
	{HASH_MULTIPLIER=65599 };
	int i;
	unsigned int uiHash=0U;
	assert (pckey != NULL); 
	for(i=0;pckey[i]!='\0';i++)
		uiHash=uiHash *(unsigned int) HASH_MULTIPLIER + (unsigned int) pckey[i];
	return(int)(uiHash%(unsigned int)ibucketcount); 

}

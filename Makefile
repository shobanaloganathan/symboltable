CC=gcc


CFLAGS=-c -Wall -std=c99 -pedantic -Wextra


all: test_linked test_hash test clean

test_linked: symtable_test.o symtable_link.o
	
	$(CC) symtable_test.o symtable_link.o -o test_linked


test_hash: symtable_test.o symtable_hash.o hash.o
	
	$(CC) symtable_test.o symtable_hash.o hash.o -o test_hash


symtable_test.o: symtable_test.c
	
	$(CC) $(CFLAGS) symtable_test.c


symtable_link.o: symtable_link.c
	
	$(CC) $(CFLAGS) symtable_link.c


symtable_hash.o: symtable_hash.c
	
	$(CC) $(CFLAGS) symtable_hash.c


hash.o: hash.c
	
	$(CC) $(CFLAGS) hash.c


test:
	./test_linked && ./test_hash


clean:
	rm -rf *o test_hash test_linked

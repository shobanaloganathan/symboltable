#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "symtable.h"
#include "hash.h"

struct node // structure for each node containing key value and pointer to next node
{
    const char *pcKey;
	const void *pvValue;
	struct node *next;
};

struct hash  // structure for hash bucket
{
    int bcount;       // total no of elements in the bucket
    struct node *next;
};

struct SymTable // structure for symtable 
{
	int ele;     // total no of element 
	int buckets;    // total no of buckets
	struct hash **htable; // pointer to the hash table
};

SymTable_t SymTable_new (void)
{
    int i;
	SymTable_t newSymTable = (SymTable_t)malloc(sizeof(struct SymTable));  // allocates memory to the symbol table

	if(newSymTable == NULL) 
		return NULL;

	newSymTable->ele=0;			// initially assigns no of elements as 0
	newSymTable->buckets= 509;  // initially assigns no of buckets as 509

	newSymTable->htable = (struct hash**)malloc(  newSymTable->buckets * sizeof(struct hash* ));  // allocates memory to the hash table 

	if(newSymTable->htable == NULL) 
		return NULL;

	for(i = 0;i < newSymTable->buckets;i++)
    {
		newSymTable->htable[i] = (struct hash*)malloc(sizeof(struct hash ));  // allocates memory for hash
    }

	for(i = 0;i < newSymTable->buckets;i++) // assigns all buckets initial value as null
    {
		newSymTable->htable[i]->bcount = 0;
		newSymTable->htable[i]->next = NULL;
    }
    return newSymTable;
}

void SymTable_free (SymTable_t oSymTable)
{
	struct node *temp;
	int i;
	assert(oSymTable);
	for(i=0;i<oSymTable->buckets;i++)   // iterates through all buckets and frees all memory
	{
		while (oSymTable->htable[i]->next)   
		{
			temp=oSymTable->htable[i]->next;
			oSymTable->htable[i]->next = oSymTable->htable[i]->next->next;
			free((char *)temp->pcKey);
			free(temp);
		}

		free (oSymTable->htable[i]);
	}

	free(oSymTable->htable);
	free (oSymTable);
}

int SymTable_getLength (SymTable_t oSymTable)
{
	assert(oSymTable);
	if(oSymTable==NULL)
		return 0;
	return (oSymTable->ele);    // returns the no of elements in the symboltable
}

void SymTable_map (SymTable_t oSymTable, void (*pfApply)(const char *pcKey,const void *pvValue,void *pvExtra),const void *pvExtra)
{
	struct node *curr;
	int i;
	assert(oSymTable);
	assert(pfApply);
	for(i=0;i<oSymTable->buckets;i++)	// iterates to each node and calls pfapply function
	{
		curr = oSymTable->htable[i]->next;
		while (curr)   
		{
			pfApply(curr->pcKey,curr->pvValue,(void *)pvExtra);
			curr = curr->next;
		}
	}
	return;
}

int SymTable_contains (SymTable_t oSymTable,const char *pcKey)  // checks whether a key is present and returns the value
{
	
	struct node *temp = oSymTable->htable[(symtable_hash(pcKey,oSymTable->buckets))]->next;
	assert(oSymTable);
	assert(pcKey);

	while(temp!=NULL)
    {
        if(!(strcmp(temp->pcKey,pcKey)))
			return 1;
		else
        temp=temp->next;
    }
    return 0;
}

void SymTable_rehash(SymTable_t oSymTable) // performs rehashing whenno of elements exceeds the no of buckets
{
	 
	int bucketcount[10] = {509, 1021, 2039, 4093, 8191, 16381, 32749, 65521};  // array to store the predefined no of bucket size
	int obcount,i,n;
	struct node *curr,*prev,*temp;
	struct hash **ot;
	assert(oSymTable);
	obcount = oSymTable->buckets;
	ot = oSymTable->htable;
	i=0;
	while(bucketcount[i]!=oSymTable->buckets)
	{
		i++;
	}
	i++;
	oSymTable->buckets=bucketcount[i];

	oSymTable->htable = (struct hash**)malloc(oSymTable->buckets * sizeof(struct hash*));
	if(!oSymTable->htable) return;
	for(i = 0;i < oSymTable->buckets;i++)
    	{
		oSymTable->htable[i] = (struct hash*)malloc(sizeof(struct hash ));
    	}
	
	for(i=0;i<oSymTable->buckets;i++)
    	{
		oSymTable->htable[i]->next = NULL;
        	oSymTable->htable[i]->bcount = 0;
    	}	
	for(i=0;i<obcount;i++)			
	{
		for(curr = ot[i]->next;curr;ot[i]->next = curr)
		{
			n = symtable_hash(curr->pcKey,oSymTable->buckets);
			
			prev = curr;
			curr = curr->next;
			prev->next = oSymTable->htable[n]->next;
			oSymTable->htable[n]->next = prev;
			oSymTable->htable[n]->bcount++;
		}

	}

	for(i=0;i<obcount;i++)		// iterates through old bucket and frees the unused memory
	{
		while (ot[i]->next)   
		{
			temp=ot[i]->next;
			ot[i]->next = ot[i]->next->next;
			free(temp);
		}
		free (ot[i]);
	}
	free(ot);
}

int SymTable_put (SymTable_t oSymTable,const char *pcKey,const void *pvValue)   // function to insert the new bindings
{
	int i;
	struct node *newb;
	assert(oSymTable);
	assert(pcKey);
	if(SymTable_contains(oSymTable,pcKey))		// checks if the key is allready present
	{
		return 0;
	}

	i = symtable_hash(pcKey, oSymTable->buckets);
	newb = (struct node *)malloc(sizeof(struct node));  
	newb->pcKey=(char *)malloc(strlen(pcKey)*sizeof(char)+1);
	
	strcpy((char *)newb->pcKey,pcKey);
	newb->pvValue = pvValue;

	newb->next = oSymTable->htable[i]->next;
	oSymTable->htable[i]->next = newb;
	oSymTable->htable[i]->bcount++;
	oSymTable->ele++;

	if(oSymTable->ele > oSymTable->buckets)  // checks whether to do rehashing if required calls the rehashing function
	{
		if(oSymTable->buckets>=65521)
			return 0;
		else

		SymTable_rehash(oSymTable);
	}
	return 1;
}

void *SymTable_get (SymTable_t oSymTable, const char *pcKey)   // function to get the value corresponding to a key
{
	struct node *temp = oSymTable->htable[(symtable_hash(pcKey,oSymTable->buckets))]->next;
	if(temp == NULL) return NULL;
	assert(oSymTable);
	assert(pcKey);

	while(temp!=NULL)
	{
		if(!(strcmp(temp->pcKey,pcKey)))
		{
			return (void *)temp->pvValue;
		}
		temp=temp->next;
	}
	return NULL;
}


void *SymTable_replace (SymTable_t oSymTable,const char *pcKey,const void *pvValue)   // function to replace the key's current value with a new one
{
	const void *v;
	struct node *temp = oSymTable->htable[(symtable_hash(pcKey,oSymTable->buckets))]->next;
	if(temp == NULL) return NULL;
	assert(oSymTable);
	assert(pcKey);
	while(temp!=NULL)
	{
		if(!(strcmp(temp->pcKey,pcKey)))
		{
			v= temp->pvValue;  
			temp->pvValue = pvValue;
			return (void *)v;
		}
		temp=temp->next;
	}
	return NULL;
}

void *SymTable_remove (SymTable_t oSymTable,const char *pcKey)   // function to remove a binding of a particular key
{
	int index;
	struct node *temp,*prev;
	assert(oSymTable!=NULL);
	assert(pcKey != NULL);

	if(SymTable_contains (oSymTable,pcKey))
	{
		const void *oldVal;
		index = symtable_hash(pcKey,oSymTable->buckets);
		for(temp=oSymTable->htable[index]->next,prev=NULL;temp;prev=temp,temp=temp->next)    // iterates through the bucket 
		{
			if(!(strcmp(temp->pcKey,pcKey)))
			{
				if(prev==NULL)
				{
					oSymTable->htable[index]->next = temp->next;
				}
				else prev->next = temp->next;
				oldVal = temp->pvValue;
				free((char *)temp->pcKey);
				free(temp);
				oSymTable->htable[index]->bcount--;
				oSymTable->ele--;
				return (void *)oldVal;
			}
		}
	}
	return NULL;
}

#include<stdio.h>
#include<malloc.h>
#include<assert.h>
#include<string.h>
#include "symtable.h"

// structure declaration of nodes in the symboltable 

struct node
{
	char *pcKey;
	void *pvValue;
	struct node *next;
};

// structure declaration for the symbol table

struct SymTable
{
	struct node *head;
};



SymTable_t SymTable_new (void)
{
	SymTable_t n=(SymTable_t)malloc(sizeof(struct node)); //creating a pointer for new symboltable
	if(n==NULL)
	return 0;
	n->head=NULL;
	return n;
}



void SymTable_free(SymTable_t oSymTable)
{ 
	struct node *p,*temp;
	assert(oSymTable);
	p=oSymTable->head;
	while(p!=NULL)  // iterates and deletes all memory allocated
	{
		temp=p->next;
		free(p->pcKey);
		free(p);
		p=temp;
	}
	free(oSymTable);
	
		
}

 

int SymTable_getLength(SymTable_t oSymTable)
{
	struct node *p;
	int count=0;
	assert(oSymTable);
	p=oSymTable->head;
	if(p==NULL)
		return 0;
	else
	{
		while(p!=NULL)
		{
			count+=1;
			p=p->next;
		}
		return count;
	}
}



void SymTable_map (SymTable_t oSymTable,  void (*pfApply)(const char *pcKey, const void *pvValue, void *pvExtra),const void *pvExtra)
{
	struct node *p;
	assert(oSymTable);
	p=oSymTable->head;
	if(p==NULL)
		return;
	else
	{
		while(p!=NULL) // iterates through the symboltable
		{
			pfApply(p->pcKey, p->pvValue, (void*)pvExtra);  // calls the function pfapply
			p=p->next;
		}
	}
}


int SymTable_contains(SymTable_t oSymTable, const char *pcKey)
{
	struct node *p;
	assert(oSymTable);
	assert(pcKey);
	p=oSymTable->head;
	while(p!=NULL)
	{
		if(!(strcmp(p->pcKey,pcKey)))
			return 1;
		p=p->next;
	}
	return 0;
}

void *SymTable_get(SymTable_t oSymTable,const char *pcKey)
{
	struct node *p;
	assert(oSymTable);
	assert(pcKey);
	p=oSymTable->head;
	while(p!=NULL)
	{
		if(!(strcmp(p->pcKey,pcKey)))
			return(p->pvValue);
		p=p->next;
	}
	return NULL;
}


int SymTable_put(SymTable_t oSymTable, const char *pcKey, const void *pvValue)
{
	struct node *p,*temp;
	assert(oSymTable);
	assert(pcKey);
	if(SymTable_contains(oSymTable,pcKey))
		return 0;
	p=oSymTable->head;
	if(oSymTable->head==NULL) // if the symboltable is initially empty
	{
		temp=(struct node*)malloc(sizeof(struct node));
		if(temp==NULL)
			return 0;
		temp->pcKey=(char *)malloc(strlen(pcKey)*sizeof(char)+1);
		if((temp->pcKey)==NULL)
			return 0;
		oSymTable->head=temp;
		strcpy(temp->pcKey,pcKey);
		temp->pvValue=(void *)pvValue;
		temp->next=NULL;
		return 1;
		
	}
	else // if symbol table is not empty
	{
		temp=(struct node*)malloc(sizeof(struct node));
		if(temp==NULL)
			return 0;
		temp->pcKey=(char *)malloc(strlen(pcKey)*sizeof(char)+1);
		if((temp->pcKey)==NULL)
			return 0;
		strcpy(temp->pcKey,pcKey);
		temp->pvValue=(void *)pvValue;
		temp->next=p->next;
		p->next=temp;	
		return 1;

	}
}


void *SymTable_replace(SymTable_t oSymTable, const char *pcKey,const void *pvValue)
{
	struct node *p;
	int check=0;
	assert(oSymTable);
	assert(pcKey);
	assert(pvValue);
	p=oSymTable->head;
	while(p!=NULL)
	{
		if(!(strcmp(p->pcKey,pcKey)))
		{
			p->pvValue=(void*)pvValue;
			check=5;
		}
		p=p->next;
	}
	if(check==0)
	return NULL;
	else 
		return ((void*)pvValue);
}

void *SymTable_remove (SymTable_t oSymTable, const char *pcKey)
{
	struct node *p,*temp;
	int check=0;
	void *val;
	assert(oSymTable);
	assert(pcKey);
	p=oSymTable->head;
	temp=p->next;
	while((p!=NULL)&&(check!=5)&&(temp!=NULL))  // if the key is the head node
	{
		if(!(strcmp(oSymTable->head->pcKey,(char *)pcKey)))
		{
			oSymTable->head=temp;
			val=oSymTable->head->pvValue;
			free(p->pcKey);
			free(p);
			check=5;
		}
		else if(!(strcmp(temp->pcKey,(char *)pcKey))) // if the key is not the head node
		{
			p->next=temp->next;
			val=temp->pvValue;
			free(temp->pcKey);
			free(temp);
			check=5;
		}
		p=p->next;
		temp=temp->next;
	}
   if(check==0)
		return NULL;
	else
		return (val);

}

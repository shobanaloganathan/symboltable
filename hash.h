#ifndef HASH_H
#define HASH_H

//header file for the hash function declaration
extern int symtable_hash(const char *pckey,int ibucketcount); // function for finding the hash value

#endif /* HASH_H*/
This project builds two versions of a symbol table -one based on linked lists and one on hash tables.Both versions provide the same level of
functionality. The link list approach has the advantage of simplicity but it does not hold good for large number of inputs which is essential for symbol table.
whereas in hash implementation it may seem time consuming for small inputs. But it has proved its efficiency when large number of inputs are given. The time taken
to search a key in link list and hash implementation differs based on the input. but when we check the over all average performance hash table
implementation is working more efficient than link list.

linklist implementationis done in the c file symtable_link.c

hash table implementation is performed in the c file symtable_hash.c

Both c files uses a common header symtable.h 

symtable_hash.c includes hash.h header file

hash function is defined in hash.c c file

There is a common test file for both link and hash table and that is done in the c file symtable_test.c

Implementation of make file:

$make will run the make file and creates 2 binaries test_linked corresponding to linklist and test_hash corresponding to hash implementation.
